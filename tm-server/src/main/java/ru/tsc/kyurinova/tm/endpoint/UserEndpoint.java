package ru.tsc.kyurinova.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.api.endpoint.IUserEndpoint;
import ru.tsc.kyurinova.tm.api.service.IServiceLocator;
import ru.tsc.kyurinova.tm.dto.model.SessionDTO;
import ru.tsc.kyurinova.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@NoArgsConstructor
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    List<UserDTO> findAllUser(
            @Nullable
            @WebParam(name = "session")
                    SessionDTO session
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findAll();
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    UserDTO findByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @NotNull
    UserDTO findByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIdUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "id", partName = "id")
                    String id
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().existsById(id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean existsByIndexUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "index", partName = "index")
                    Integer index
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().existsByIndex(index);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    UserDTO findByEmailUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByEmail(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void isLoginExistsUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().isLoginExists(login);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void isEmailExists(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "email", partName = "email")
                    String email
    ) {
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getUserService().isEmailExists(email);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public @Nullable
    UserDTO findByLoginUser(
            @Nullable
            @WebParam(name = "session", partName = "session")
                    SessionDTO session,
            @Nullable
            @WebParam(name = "login", partName = "login")
                    String login
    ) {
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getUserService().findByLogin(login);
    }

}
