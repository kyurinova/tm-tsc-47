package ru.tsc.kyurinova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kyurinova.tm.command.AbstractCommand;
import ru.tsc.kyurinova.tm.endpoint.SessionDTO;

public class DataXmlSaveFasterXMLCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "data-xml-save";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML SAVE]");
        @Nullable final SessionDTO session = serviceLocator.getSessionService().getSession();
        serviceLocator.getAdminDataEndpoint().dataXmlSaveFasterXML(session);
    }

}
